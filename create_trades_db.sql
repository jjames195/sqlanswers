DROP TABLE price_point;
DROP TABLE position;
DROP TABLE trader;
DROP TABLE trade;

CREATE TABLE trade
(
  ID INTEGER NOT NULL,
  instant DATETIME NOT NULL,
  stock VARCHAR(8) NOT NULL,
  buy BIT NOT NULL,
  size INTEGER NOT NULL,
  price DECIMAL(12,4) NOT NULL,
  PRIMARY KEY (ID)
);

CREATE TABLE trader
(
  ID INTEGER NOT NULL,
  first_name VARCHAR(24) NOT NULL,
  last_name VARCHAR(24) NOT NULL,
  PRIMARY KEY (ID)
);

CREATE TABLE position
(
  ID INTEGER NOT NULL,
  trader_ID INTEGER NOT NULL,
  opening_trade_ID INTEGER NOT NULL,
  closing_trade_ID INTEGER,
  PRIMARY KEY (ID),
  CONSTRAINT position_trader
    FOREIGN KEY (trader_ID) REFERENCES trader (ID),
  CONSTRAINT position_trade_1
    FOREIGN KEY (opening_trade_ID) REFERENCES trade (ID),
  CONSTRAINT position_trade_2
    FOREIGN KEY (closing_trade_ID) REFERENCES trade (ID)
);

CREATE TABLE price_point
(
  ID INTEGER,
  stock VARCHAR(8) NOT NULL,
  instant DATETIME NOT NULL,
  opn DECIMAL(12,4) NOT NULL,
  high DECIMAL(12,4) NOT NULL,
  low DECIMAL(12,4) NOT NULL,
  cls DECIMAL(12,4) NOT NULL,
  volume INTEGER NOT NULL,
  PRIMARY KEY (ID)
);
