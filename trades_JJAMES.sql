select case when trade.buy = 1 then 'BUY' else 'SELL' end, trade.size, trade.price
from trade
  join position on position.opening_trade_id = trade.id or position.closing_trade_id = trade.id
where position.trader_id = 1 and trade.stock = 'MRK'
order by trade.instant;


select sum(trade.size * trade.price * (case when trade.buy=1 then -1 else 1 end))
from trade
join position on position.opening_trade_id = trade.id or position.closing_trade_id = trade.id
WHERE 
position.trader_id = 3
and trade.stock = 'MRK'
and position.closing_trade_id != position.opening_trade_id
and position.closing_trade_id is not null;